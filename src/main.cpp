
#include <Homie.h>
#include <Arduino.h>

const int UPDATE_INTERVAL = 300;
const int ALARM_PIN = 4;
const int POWER_PIN = 5;

ADC_MODE(ADC_VCC);

HomieNode moistureNode("moisture", "alarm");
HomieNode batteryNode("battery", "level");

void setupHandler()
{
    pinMode(ALARM_PIN, INPUT);
    pinMode(POWER_PIN, OUTPUT);
    digitalWrite(POWER_PIN, LOW);
}

void loopHandler()
{
    // update moisture alarm
    digitalWrite(POWER_PIN, HIGH);
    delay(20);
    Homie.setNodeProperty(moistureNode, "alarm", String(digitalRead(ALARM_PIN)));
    digitalWrite(POWER_PIN, LOW);

    // update battery level
    Homie.setNodeProperty(batteryNode, "level", String(float(ESP.getVcc())/1000.0));

    // deep sleep
    int sleep_time = (UPDATE_INTERVAL*1000000UL) - micros();
    ESP.deepSleep(sleep_time, WAKE_RF_DEFAULT);
}

void setup()
{
    Homie.setFirmware("homie-plantmon", "0.1.0");
    Homie.registerNode(moistureNode);
    Homie.registerNode(batteryNode);
    Homie.setSetupFunction(setupHandler);
    Homie.setLoopFunction(loopHandler);
    Homie.setup();
}

void loop()
{
    Homie.loop();
}
